import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';

export class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      if(App.loadingElement != null){
      App.loadingElement.style.visibility = 'visible';

      const fighters = await fighterService.getFighters();
      const fightersElement = createFighters(fighters);

      App.rootElement?.appendChild(fightersElement);
      }
    } catch (error) {
      console.warn(error);
      if(App.rootElement != null){
      App.rootElement.innerText = 'Failed to load data';
      }
    } finally {
      if(App.loadingElement != null){
        App.loadingElement.style.visibility = 'hidden';
      ]
    }
  }
}